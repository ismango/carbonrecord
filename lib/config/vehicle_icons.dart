import 'package:flutter/material.dart';

Widget getLeadingIcon(String vehicle) {
  switch (vehicle) {
    case '公交车':
      return const Icon(Icons.directions_bus);
    case '小汽车':
      return const Icon(Icons.car_rental);
    case '摩托车':
      return const Icon(Icons.motorcycle);
    case '飞机':
      return const Icon(Icons.airplanemode_active);
    case '火车':
      return const Icon(Icons.train);
    case '高铁':
      return const Icon(Icons.directions_railway);
    case '地铁':
      return const Icon(Icons.subway);
    case '轻轨':
      return const Icon(Icons.directions_subway);
    case '轮船':
      return const Icon(Icons.directions_boat);
  //  自定义
    default:
      return const Icon(Icons.dashboard_customize);
  }
}