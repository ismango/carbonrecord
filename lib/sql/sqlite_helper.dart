import 'dart:io';
import 'dart:math';
import 'package:carbonrecord/bean/category.dart';
import 'package:carbonrecord/bean/daily_emission.dart';
import 'package:carbonrecord/bean/level_emission.dart';
import 'package:carbonrecord/bean/object.dart';
import 'package:carbonrecord/bean/customize_activity.dart';
import 'package:carbonrecord/bean/record.dart';
import 'package:carbonrecord/bean/total_average.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../bean/customizeWay.dart';

import '../bean/customize_activity_vehicle.dart';

class SqliteHelper {

  SqliteHelper._internal();

  static final SqliteHelper instance = SqliteHelper._internal();


  factory SqliteHelper() =>  _instance;

  static final SqliteHelper _instance = SqliteHelper._internal();

  Database? _database;

  Future<Database> get database async{
    return _database ??= await initDB();
  }

  DateFormat dateFormat = DateFormat('yyyy-MM-dd');
  DateFormat timeFormat = DateFormat('yyy-MM-dd HH:mm:ss');

  initDB() async {
    var databasesPath = await getDatabasesPath();
    var exist = await File('$databasesPath/carbon_transport.db').exists();
    var path = join(databasesPath, 'carbon_transport.db');

    if (!exist) {
      await deleteDatabase(path);

      try {
        await Directory(dirname(path)).create(recursive: true);
      } catch (_) {}

      var data = await rootBundle.load(join('assets', 'carbon_transport.db'));

      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(path).writeAsBytes(bytes, flush: true);
    }
    return await openDatabase(path);
  }

  Future<List<Record>> getRecordList(
      DateTime startTime, DateTime endTime) async {
    final db = await database;
    List<Map> maps = await db.rawQuery(
        'select id, customize_id, vehicle_name, distance, emission_result, unit_name, update_time from (select r.id, null as customize_id, v.vehicle_name, r.distance, r.emission_result, u.unit_name, r.update_time from records r, vehicles v, units u where r.vehicle_id = v.id and v.unit_id = u.id and r.customize_id = 0 union all select null as id, r.customize_id, cr.customize_name as vehicle_name, sum(r.distance) as distance, sum(r.emission_result) as emission_result, u.unit_name, r.update_time from records r, customize_records cr, vehicles v, units u where r.customize_id = cr.id and r.vehicle_id = v.id and v.unit_id = u.id group by r.customize_id) where update_time between ? and ? order by update_time desc;',
        [timeFormat.format(startTime), timeFormat.format(endTime)]);
    List<Record> recordList = List.generate(maps.length, (index) {
      return Record(
        id: maps[index]['id'] ?? -1,
        customizeId: maps[index]['customize_id'] ?? -1,
        vehicleName: maps[index]['vehicle_name'],
        distance: maps[index]['distance'],
        emissionResult: maps[index]['emission_result'],
        unitName: maps[index]['unit_name'],
        updateTime: maps[index]['update_time'],
      );
    });
    return recordList;
  }

  Future<List<customizeWay>> getCustomizeWayList() async {
    final db = await database;
    List<Map> maps = await db.rawQuery('SELECT id,customize_way FROM customize_ways;');
    List<customizeWay> customizeWayList = List.generate(maps.length, (index) {
      return customizeWay(
        id: maps[index]['id'],
        customizeWayName: maps[index]['customize_way'],
      );
    });
    return customizeWayList;
  }

  Future<List<Category1>> getCategoryList() async {
    final db = await database;
    List<Map> maps = await db.rawQuery('SELECT id, category_name FROM categories;');
    List<Category1> categoryList = List.generate(maps.length, (index) {
      return Category1(id: maps[index]['id'], categoryName: maps[index]['category_name']);
    });
    return categoryList;
  }

  Future<List<Object1>> getObjectList(int categoryId) async {
    final db = await database;
    List<Map> maps = await db.rawQuery('SELECT id,vehicle_name FROM vehicles WHERE category_id = ?;', [categoryId]);
    List<Object1> objectList = List.generate(maps.length, (index) {
      return Object1(id: maps[index]['id'], vehicleName: maps[index]['vehicle_name']);
    });
    return objectList;
  }


  Future<TotalAverage> getTotalAverage(DateTime startTime, DateTime endTime) async {
    final db = await database;
    List<Map> maps = await db.rawQuery(
        'select sum(emission_result) as total_emission_result from records where update_time between ? and ?;',
        [timeFormat.format(startTime), timeFormat.format(endTime)]);
    return TotalAverage(
        totalEmissionResult: maps[0]['total_emission_result'] ?? 0,
        averageEmissionResult: maps[0]['total_emission_result']==null ? 0 : maps[0]['total_emission_result']/((endTime.difference(startTime)).inDays + 1),
        treeCount: 0
    );
  }

  Future<int> deleteRecordOrActivity(Record record) async{
    final db = await database;
    // 删除记录
    if (record.customizeId == -1) {
      return await db.delete('records', where: 'id=?', whereArgs: [record.id]);
    }
    // 删除活动
    else {
      // 删除记录表
      await db.delete('records',
          where: 'customize_id=?', whereArgs: [record.customizeId]);
      // 删除活动表
      return await db.delete('customize_records',
          where: 'id=?', whereArgs: [record.customizeId]);
    }
  }

  Future<List<CustomizeActivity>> getCustomizeActivityList() async{
    final db = await database;
    List<Map> maps = await db.rawQuery(
        'select cw.id, cw.customize_way as activity_name,  count(cv.id) as vehicle_count from customize_ways cw, customize_vehicles cv where cw.id = cv.customize_way_id group by customize_way_id;');
    List<Map> nullMaps = await db.rawQuery(
      'select cw.id, cw.customize_way as activity_name, 0 as vehicle_count from customize_ways cw where cw.id not in (select distinct(customize_way_id) from customize_vehicles);');
    maps += nullMaps;

    List<CustomizeActivity> customizeActivityList = List.generate(maps.length, (index) {
      return CustomizeActivity(
        id: maps[index]['id'],
        activityName: maps[index]['activity_name'],
        vehicleCount: maps[index]['vehicle_count'],
      );
    });
    return customizeActivityList;
  }

  Future<Map<int, List<CustomizeActivityVehicle>>> getCustomizeActivityVehicleMap(List<CustomizeActivity> customizeActivityList) async{
    final db = await database;
    Map<int, List<CustomizeActivityVehicle>> map = {};
    for(CustomizeActivity customizeActivity in customizeActivityList){
      List<Map> maps = await db.rawQuery(
          'select cv.id, cv.vehicle_id, cv.distance, cv.customize_way_id as customize_activity_id, v.vehicle_name, u.unit_name from customize_vehicles cv, vehicles v, units u where cv.vehicle_id = v.id and v.unit_id = u.id and cv.customize_way_id = ?;', [customizeActivity.id]);
      List<CustomizeActivityVehicle> customizeActivityVehicleList = List.generate(maps.length, (index) {
        return CustomizeActivityVehicle(
          id: maps[index]['id'],
          vehicleId: maps[index]['vehicle_id'],
          distance: maps[index]['distance'],
          customizeActivityId: maps[index]['customize_activity_id'],
          vehicleName: maps[index]['vehicle_name'],
          unitName: maps[index]['unit_name'],
        );
      });
      map[customizeActivity.id] = customizeActivityVehicleList;
    }
    return map;
  }

  Future<int> deleteCustomizeActivity(int customizeActivityId) async{
    final db = await database;
    // 删除自定义活动中的交通工具
    await db.delete('customize_vehicles', where: 'customize_way_id=?', whereArgs:[customizeActivityId]);
    // 删除自定义活动
    return await db.delete('customize_ways', where: 'id=?', whereArgs:[customizeActivityId]);
  }

  Future<int> deleteCustomizeActivityVehicle(int customizeVehicleId) async{
    final db = await database;
    // 删除自定义活动中的交通工具
    return await db.delete('customize_vehicles', where: 'id=?', whereArgs:[customizeVehicleId]);
  }

  Future<int> insertCustomizeActivityVehicle(String vehicleName) async{
    final db = await database;
    return await db.insert('customize_ways', {'customize_way': vehicleName});
  }

  // Future<TotalAverage> getTotalEmission(DateTime startTime, DateTime endTime) async{
  //   final db = await database;
  //   List<Map> maps = await db.rawQuery('select sum(emission_result) as total_emission_result from records where update_time between ? and ?;',
  //       [timeFormat.format(startTime), timeFormat.format(endTime)]);
  //   double totalEmissionResult = maps[0]['total_emission_result']??0;
  //   double averageEmissionResult = totalEmissionResult/((endTime.difference(startTime)).inDays + 1);
  //   int treeCount = (averageEmissionResult/0.055).ceil();
  //   return TotalAverage(
  //       totalEmissionResult: totalEmissionResult,
  //       averageEmissionResult: averageEmissionResult,
  //       treeCount: treeCount);
  // }

  Future<TotalAverage> getTotalEmission(DateTime startTime, DateTime endTime, String categoryName) async{
    final db = await database;
    List<Map> maps = [];
    if(categoryName == "全部"){
      maps = await db.rawQuery('select sum(emission_result) as total_emission_result from records where update_time between ? and ?;',
          [timeFormat.format(startTime), timeFormat.format(endTime)]);
    }
    else{
      maps = await db.rawQuery('select sum(emission_result) as total_emission_result from records r, vehicles v, categories c where r.vehicle_id = v.id and v.category_id = c.id and c.category_name = ? and r.update_time between ? and ?;',
          [categoryName, timeFormat.format(startTime), timeFormat.format(endTime)]);
    }
    double totalEmissionResult = maps[0]['total_emission_result']??0;
    double averageEmissionResult = totalEmissionResult/((endTime.difference(startTime)).inDays + 1);
    int treeCount = (averageEmissionResult/0.055).ceil();
    return TotalAverage(
        totalEmissionResult: totalEmissionResult,
        averageEmissionResult: averageEmissionResult,
        treeCount: treeCount);
  }

  // Future<List<LevelEmission>> getLevelEmission(DateTime startTime, DateTime endTime) async{
  //   final db = await database;
  //   List<Map> maps = await db.rawQuery('select sum(emission_result) as total_emission_result from records r, vehicles v, categories c where r.vehicle_id = v.id and v.category_id = c.id and c.category_name = ? and r.update_time between ? and ?;',
  //       [timeFormat.format(startTime), timeFormat.format(endTime)]);
  //   List<LevelEmission> levelEmissionList = List.generate(maps.length, (index) {
  //     return LevelEmission(
  //       categoryName: maps[index]['category_name'],
  //       emissionResult: maps[index]['emission_result'],
  //     );
  //   });
  //   return levelEmissionList;
  // }

  Future<List<LevelEmission>> getLevelEmission(DateTime startTime, DateTime endTime, String categoryName) async{
    final db = await database;
    List<Map> maps = [];
    if(categoryName == "全部"){
      maps = await db.rawQuery('select sum(emission_result) as total_emission_result from records r, vehicles v, categories c where r.vehicle_id = v.id and v.category_id = c.id and c.category_name = ? and r.update_time between ? and ?;',
          [timeFormat.format(startTime), timeFormat.format(endTime)]);
    }
    else{
      maps = await db.rawQuery('select sum(emission_result) as emission_result, v.vehicle_name as category_name from records r, vehicles v, categories c where r.vehicle_id = v.id and v.category_id = c.id and c.category_name=? and r.update_time between ? and ? group by v.id;',
          [categoryName, timeFormat.format(startTime), timeFormat.format(endTime)]);
    }
    List<LevelEmission> levelEmissionList = List.generate(maps.length, (index) {
      return LevelEmission(
        categoryName: maps[index]['category_name'],
        emissionResult: maps[index]['emission_result'],
      );
    });
    return levelEmissionList;
  }

  Future<Map<String, double>> getPieChartDataMap(DateTime startTime, DateTime endTime, String categoryName) async {
    final db = await database;
    List<Map> maps = [];
    Map<String, double> pieChartDataMap = {};
    if(categoryName == "全部"){
      maps = await db.rawQuery('select sum(emission_result) as emission_result from records where update_time between ? and ?;',
          [timeFormat.format(startTime), timeFormat.format(endTime)]);
      pieChartDataMap['全部'] = maps[0]['emission_result']??0;
    }
    else{
      maps = await db.rawQuery('select sum(emission_result) as emission_result, v.vehicle_name as category_name from records r, vehicles v, categories c where r.vehicle_id = v.id and v.category_id = c.id and c.category_name=? and r.update_time between ? and ? group by v.id;',
          [categoryName, timeFormat.format(startTime), timeFormat.format(endTime)]);
      for(Map map in maps){
        pieChartDataMap[map['category_name']] = map['emission_result'];
      }
    }
    return pieChartDataMap;
  }

  Future<List<DailyEmission>> getLineChartDataList(DateTime startTime, DateTime endTime) async{
    final db = await database;
    List<Map> maps = await db.rawQuery('select date(update_time) as date, sum(emission_result) as emissionResult from records where update_time between ? and ? group by date(update_time);',
    [timeFormat.format(startTime), timeFormat.format(endTime)]);
    Map<DateTime, double> lineChartDataMap = {};
    List<DailyEmission> lineChartDataList = [];
    DateTime curr=startTime;
    while(curr.isBefore(endTime)){
      lineChartDataMap[curr] = 0;
      curr = curr.add(const Duration(days: 1));
    }
    for(Map map in maps){
      lineChartDataMap[DateTime.parse(map['date'])] = map['emissionResult'];
    }
    lineChartDataMap.forEach((key, value) {
      lineChartDataList.add(DailyEmission(datetime: key, emissionResult: value));
    });
    return lineChartDataList;
  }
}
