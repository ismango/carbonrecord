class CustomizeActivity{
  int id;
  String activityName;
  int vehicleCount;

  CustomizeActivity({required this.id, required this.activityName, required this.vehicleCount});

  Map<String, dynamic> toMap(){
    final Map<String, dynamic> map = {};
    map['id'] = id;
    map['activity_name'] = activityName;
    map['vehicle_count'] = vehicleCount;
    return map;
  }

  @override
  String toString() {
    return 'CustomizeActivity{id:$id, activityName:$activityName, vehicleCount:$vehicleCount}';
  }
}