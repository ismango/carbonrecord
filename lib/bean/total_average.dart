class TotalAverage{
  double totalEmissionResult;
  double averageEmissionResult;
  int treeCount;

  TotalAverage({required this.totalEmissionResult, required this.averageEmissionResult, required this.treeCount});

  Map<String, dynamic> toMap(){
    final Map<String, dynamic> map = {};
    map['total_emission_result'] = totalEmissionResult;
    map['average_emission_result'] = averageEmissionResult;
    map['tree_count'] = treeCount;
    return map;
  }

  @override
  String toString() {
    return 'TotalAverage{totalEmissionResult:$totalEmissionResult, averageEmissionResult:$averageEmissionResult, treeCount$treeCount}';
  }
}