class CustomizeActivityVehicle{
  int id;
  int vehicleId;
  double distance;
  int customizeActivityId;
  String vehicleName;
  String unitName;

  CustomizeActivityVehicle({required this.id, required this.vehicleId,
    required this.distance, required this.customizeActivityId,
    required this.vehicleName, required this.unitName});

  Map<String, dynamic> toMap(){
    final Map<String, dynamic> map = {};
    map['id'] = id;
    map['vehicle_id'] = vehicleId;
    map['distance'] = distance;
    map['customize_activity_id'] = customizeActivityId;
    map['vehicle_name'] = vehicleName;
    map['unit_name'] = unitName;
    return map;
  }

  @override
  String toString() {
    return 'CustomizeActivityVehicle{id:$id, vehicle_id:$vehicleId, distance:$distance,'
        'customize_activity_id:$customizeActivityId, vehicle_name:$vehicleName, unit_name:$unitName}';
  }
}