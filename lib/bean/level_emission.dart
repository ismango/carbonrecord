class LevelEmission{
  String categoryName;
  double emissionResult;

  LevelEmission({required this.categoryName, required this.emissionResult});

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = {};
    map['category_name'] = categoryName;
    map['emission_result'] = emissionResult;
    return map;
  }

  @override
  String toString() {
    return 'LevelEmission{category_name:$categoryName, emission_result:$emissionResult}';
  }
}