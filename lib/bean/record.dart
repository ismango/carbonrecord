class Record{
  int id;
  int customizeId;
  String vehicleName;
  double distance;
  double emissionResult;
  String unitName;
  String updateTime;

  Record({required this.id, required this.customizeId, required this.vehicleName, required this.distance,
    required this.emissionResult, required this.unitName, required this.updateTime});

  Map<String, dynamic> toMap(){
    final Map<String, dynamic> map = {};
    map['id'] = id;
    map['customize_id'] = customizeId;
    map['vehicle_name'] = vehicleName;
    map['distance'] = distance;
    map['emissionResult'] = emissionResult;
    map['unitName'] = unitName;
    map['updateTime'] = updateTime;
    return map;
  }

  @override
  String toString() {
    return 'Record{id:$id, customizeId:$customizeId, vehicleName:$vehicleName, distance:$distance, emissionResult:$emissionResult, unitName:$unitName, updateTime:$updateTime}';
  }
}