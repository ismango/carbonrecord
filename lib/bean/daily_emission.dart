class DailyEmission{
  DateTime datetime;
  double emissionResult;

  DailyEmission({required this.datetime, required this.emissionResult});

  Map<String, dynamic> toMap(){
    final Map<String, dynamic> map = {};
    map['datetime'] = datetime;
    map['emission_result'] = emissionResult;
    return map;
  }

  @override
  String toString() {
    return 'DailyEmission{datetime:$datetime, emissionResult:$emissionResult}';
  }
}