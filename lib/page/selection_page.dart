import 'package:carbonrecord/bean/customizeWay.dart';
import 'package:carbonrecord/bean/category.dart';
import 'package:carbonrecord/bean/object.dart';
import 'package:carbonrecord/page/addcustomize_page.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:carbonrecord/sql/sqlite_helper.dart';
// import '../utils//sqlite_handler.dart';
import 'addobject_page.dart';

class SelectionPage extends StatefulWidget {
  const SelectionPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SelectionPageState();
}

class _SelectionPageState extends State<SelectionPage> {
  List<customizeWay> customizeWays = [];
  List<Category1> categories = [];
  // List categories_list = [];
  // List customize_ways_list = [];
  Map<int, List<Object1>> object_map = {};

  @override
  void initState() {
    _query_object();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(true),
          ),
          title: const Text('分类'),
          centerTitle: true,
        ),
        body: Column(
          children: <Widget>[
            customizeWays.isEmpty
                ? _showText('当前没有自定义出行方式！')
                : _showCustomize(),
            categories.isEmpty
                ? _showText('当前没有出行方式！')
                : _showCategories()
          ],
        )
    );
  }

  //从缓存目录读取db文件里面的数据
  _query_object() async {
    // var db = await SqliteHandler.getDB('carbon_transport.db');
    // categories_list = await db.rawQuery('SELECT * FROM categories;');
    // for (var category_map_temp in categories_list) {
    //   int category_id_temp = category_map_temp['id'];
    //   object_map[category_id_temp] = await db.rawQuery(
    //       'SELECT * FROM vehicles WHERE category_id = ?;', [category_id_temp]);
    // }
    categories.clear();
    categories = await SqliteHelper.instance.getCategoryList();

    for (var categoryMapTemp in categories) {
      object_map[categoryMapTemp.id] = await SqliteHelper.instance.getObjectList(categoryMapTemp.id);
    }


    // customize_ways_list = await db.rawQuery('SELECT * FROM customize_ways;');
    customizeWays.clear();
    customizeWays = await SqliteHelper.instance.getCustomizeWayList();

    setState(() {});
    // await db.close();
  }

  Widget _showText(String s){
    return Text(s, textAlign: TextAlign.center);
  }

  Widget _showCustomize(){
    return ExpansionTile(
      title: const Text("自定义"),
      children: [
        ListView.builder(
          shrinkWrap: true,
          itemCount: customizeWays.length,
          itemBuilder: (context, index3) {
            return ListTile(
              title: Text(
                  customizeWays[index3].customizeWayName,
                  textAlign: TextAlign.center),
              onTap: () async {
                final bool value =
                await Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (context) => AddcustomizePage(
                        customize_id:
                        customizeWays[index3].id,
                        customize_name:
                        customizeWays[index3].customizeWayName,
                      )),
                );
                if (value) {
                  Navigator.of(context).pop(value);
                }
              },
            );
          },
        )
      ],
    );
  }

  Widget _showCategories(){
    return Expanded(
      child: (ListView.builder(
        shrinkWrap: true,
        itemCount: categories.length,
        itemBuilder: (context, index) {
          return ExpansionTile(
            title: Text(
                categories[index].categoryName),
            children: [
              ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: object_map[categories[index].id]!.length,
                itemBuilder: (context, index2) {
                  return ListTile(
                    title: Text(
                        object_map[categories[index].id]![index2].vehicleName,
                        textAlign: TextAlign.center),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AddobjectPage(
                              objectid: object_map[categories[index].id]![index2].id,
                            )),
                      );
                    },
                  );
                })
            ],
          );
        },
      )),
    );
  }


}
