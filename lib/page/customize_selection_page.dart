import 'package:carbonrecord/page/customize_add_object_page.dart';
import 'package:flutter/material.dart';

import '../utils/sqlite_handler.dart';

class CustomizeSelectionPage extends StatefulWidget {

  int customizeWayId;

  CustomizeSelectionPage({Key? key, required this.customizeWayId}) : super(key: key);

  @override
  State<CustomizeSelectionPage> createState() => _CustomizeSelectionPageState();
}

class _CustomizeSelectionPageState extends State<CustomizeSelectionPage> {
  List categories_list = [];
  List customize_ways_list = [];
  Map<int,List> object_map = {};

  @override
  void initState() {
    _query_object();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () => Navigator.of(context).pop(true),
        ),
        title: const Text('分类'),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          categories_list.isEmpty
          ?Text('当前没有数据！',textAlign: TextAlign.center)
          :Expanded(child: _showCustomizeSelection()),
        ],
      )
    );
}
  //从缓存目录读取db文件里面的数据
  _query_object() async {
    var db = await SqliteHandler.getDB('carbon_transport.db');
    categories_list = await db.rawQuery('SELECT * FROM categories;');
    for (var category_map_temp in categories_list){
      int category_id_temp = category_map_temp['id'];
      object_map[category_id_temp] = await db.rawQuery('SELECT * FROM vehicles WHERE category_id = ?;', [category_id_temp]);
    }
    customize_ways_list = await db.rawQuery('SELECT * FROM customize_ways;');
    setState(() {});
  }

  _showCustomizeSelection(){
    return ListView.builder(
      shrinkWrap: true,
      itemCount: categories_list.length,
      itemBuilder: (context,index){
        return ExpansionTile(
          title: Text('${categories_list[index]['category_name']}'),
          children: [
            ListView.builder(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              itemCount: object_map[categories_list[index]['id']]?.length,
              itemBuilder: (context,index2){
                return ListTile(
                  title: Text('${object_map[categories_list[index]['id']]![index2]['vehicle_name']}',textAlign: TextAlign.center),
                  onTap: () async {
                    final bool value = await Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => CustomizeAddObjectPage(customizeWayId: widget.customizeWayId, objectId: object_map[categories_list[index]['id']]![index2]['id'],)),
                    );
                    if(value == true){
                      Navigator.of(context).pop(value);
                    }
                  },
                );
              },
            )
          ],
        );
      },
    );
  }
}
