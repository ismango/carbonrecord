import 'package:carbonrecord/bean/category.dart';
import 'package:carbonrecord/bean/level_emission.dart';
import 'package:carbonrecord/bean/total_average.dart';
import 'package:carbonrecord/sql/sqlite_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:intl/intl.dart';
import 'package:pie_chart/pie_chart.dart';
import '../bean/daily_emission.dart';

class AnalysisPage extends StatefulWidget {
  const AnalysisPage({Key? key}) : super(key: key);

  @override
  State<AnalysisPage> createState() => _AnalysisPageState();
}

class _AnalysisPageState extends State<AnalysisPage> {

  DateFormat dateFormat = DateFormat('yyyy-MM-dd');

  DateTime now = DateTime.now();
  DateTime endTime = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day)
      .add(const Duration(hours: 23, minutes: 59, seconds: 59));
  DateTime startTime = DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day)
      .add(const Duration(hours: 23, minutes: 59, seconds: 59)).add(const Duration(days: -30, seconds: 1));

  int treeDays = 0;

  TotalAverage totalAverage = TotalAverage(totalEmissionResult: 0, averageEmissionResult: 0, treeCount: 0);
  LevelEmission levelEmission = LevelEmission(categoryName: "全部", emissionResult: 0);

  Map<String, double> pieChartDataMap = {};
  List<DailyEmission> lineChartDataList = [];

  var category = '全部';

  var dropDownValues = ['全部'];


  @override
  void initState() {
    endTime = DateTime(now.year, now.month, now.day).add(const Duration(hours: 23, minutes: 59, seconds: 59));
    startTime = endTime.add(const Duration(days: -30, seconds: 1));

      _queryData(category);
  }

  Future _queryData(String category) async {

    totalAverage = await SqliteHelper.instance.getTotalEmission(startTime, endTime, category);
    pieChartDataMap = await SqliteHelper.instance.getPieChartDataMap(startTime, endTime, category);
    lineChartDataList = await SqliteHelper.instance.getLineChartDataList(startTime, endTime);

    dropDownValues = ['全部'];
    List<Category1> categoryList = await SqliteHelper.instance.getCategoryList();
    for(Category1 category in categoryList){
      dropDownValues.add(category.categoryName);
    }

    treeDays = totalAverage.totalEmissionResult==0 ? 0 : endTime.difference(startTime).inDays+1;

    setState(() {});

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: getTimePicker(),
          centerTitle: true,
      ),
      body: Center(
          child: ListView(
        // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          // 总计日均
          getTotalAverage(),
          getTree(),
          getPieChart(),
          getDailyStatistics()
        ],
      )
      ),
    );
  }

  Widget getTotalAverage(){
    return Card(
      color: Colors.white,
      shape: const RoundedRectangleBorder(
        borderRadius:
        BorderRadius.all(Radius.circular(10.0)),
      ),
      clipBehavior: Clip.antiAlias,
      margin: const EdgeInsets.all(6.0),
      child: Container(
        alignment: Alignment.center,
        height: 150,
        width: double.infinity,
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(12.0))
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    getData('总碳排量'),
                    getData("${totalAverage.totalEmissionResult.toStringAsFixed(2)}  kg"),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    getData("日均碳排量"),
                    getData("${totalAverage.averageEmissionResult.toStringAsFixed(2)}  kg"),
                  ],
                )
              ],
            ),
            Text(
              '总碳排量约等于 ${totalAverage.treeCount} 棵树 $treeDays 天吸收的CO2',
              style: const TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget getTree(){
    return Card(
      color: Colors.white,
      shape: const RoundedRectangleBorder(
        borderRadius:
        BorderRadius.all(Radius.circular(10.0)),
      ),
      clipBehavior: Clip.antiAlias,
      margin: const EdgeInsets.all(6.0),
      child: Container(
        color: Colors.white10,
        child:Center(
            child: InteractiveViewer(
              boundaryMargin: const EdgeInsets.all(double.infinity),
              minScale: 0.2,
              maxScale: 2,
              child: SizedBox(
                  width: double.infinity,
                  height: 300,
                  child:getMapWidget(totalAverage.treeCount)
              ),
            )
        ),
      ),
    );
  }

  Widget getPieChart(){
    return Card(
        color: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius:
          BorderRadius.all(Radius.circular(10.0)),
        ),
        clipBehavior: Clip.antiAlias,
        margin: const EdgeInsets.all(6.0),
        child: SizedBox(
          width: double.infinity,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topRight,
                // 分类按钮
                child: Container(
                  height: 40,
                  width: 100,
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(12.0))
                  ),
                  child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                        value: category,
                        // icon: const Icon(Icons.arrow_downward),
                        elevation: 16,
                        // style: const TextStyle(color: Colors.deepPurple),
                        borderRadius: const BorderRadius.all(Radius.circular(15.0)),
                        underline: Container(
                          height: 2,
                          color: Colors.deepPurpleAccent,
                        ),
                        onChanged: (String? newValue) {
                          category = newValue!;
                          _queryData(category);
                          setState(() {
                            category = newValue.toString();
                          });
                        },
                        items: dropDownValues
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      )),
                ),),

              // 饼图
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(

                  child:
                  pieChartDataMap.isEmpty
                      ? const Text('没有数据哦！')
                      :// 饼图
                  PieChart(
                    dataMap: pieChartDataMap,
                    chartRadius: MediaQuery.of(context).size.width / 1.5,
                    legendOptions: const LegendOptions(
                      legendPosition: LegendPosition.bottom,
                      showLegendsInRow: true,
                    ),
                    chartValuesOptions: const ChartValuesOptions(
                        showChartValuesOutside: true,
                        decimalPlaces: 2
                    ),
                  ),
                ),
              )
            ],
          ),
        )
    );
  }

  Widget getDailyStatistics(){
    return Card(
        color: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius:
          BorderRadius.all(Radius.circular(10.0)),
        ),
        clipBehavior: Clip.antiAlias,
        margin: const EdgeInsets.all(6.0),
        child: Column(
          children: [
            Container(
              alignment: Alignment.center,
              height: 50,
              width: double.infinity,
              child: const Text(
                "每日碳排量",
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              width: double.infinity,
              child: lineChartDataList.isEmpty
                  ? const Text("没有数据哦")
                  : ListView.separated(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: lineChartDataList.length+1,
                itemBuilder: (context, index){
                  if(index == 0){
                    return const SizedBox(
                      height: 35,
                      child: ListTile(
                        leading: Text("日期"),
                        trailing: Text("碳排量(kgCO2)"),
                      ),
                    );
                  }
                  else{
                    index -= 1;
                    return SizedBox(
                      height: 35,
                      child: ListTile(
                        leading: Text(dateFormat.format(lineChartDataList[index].datetime).toString()),
                        trailing: Text(lineChartDataList[index].emissionResult.toStringAsFixed(2)),
                      ),
                    );
                  }
                },
                separatorBuilder: (BuildContext context, int index) {
                  return const Divider(color: Colors.grey,);
                },
              ),
            ),
          ],
        )
    );
  }

  Future showTimeConfirmDialog() {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('提示'),
            content: const Text('开始时间不能晚于结束日期'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(() {});
                },
                child: const Text('确定'),
              )
            ],
          );
        });
  }

  getTimePicker(){
    return Container(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    showTitleActions: true,
                    maxTime: DateTime.now(), onConfirm: (date) {
                      if (date.isAfter(endTime)) {
                        showTimeConfirmDialog();
                      } else {
                        startTime = DateTime(date.year, date.month, date.day);
                        _queryData(category);
                        setState(() {});
                      }
                    }, currentTime: startTime, locale: LocaleType.zh);
              },
              child: Text(
                dateFormat.format(startTime),
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18.0
                ),
              )),
          // const Text(':'),
          // 结束时间
          TextButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    showTitleActions: true,
                    maxTime: DateTime.now(), onConfirm: (date) {
                      if (date.isBefore(startTime)) {
                        showTimeConfirmDialog();
                      } else {
                        endTime = DateTime(date.year, date.month, date.day).add(
                            const Duration(hours: 23, minutes: 59, seconds: 59));
                        _queryData(category);
                        setState(() {});
                      }
                    }, currentTime: endTime, locale: LocaleType.zh);
              },
              child: Text(
                dateFormat.format(endTime),
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18.0
                ),
              ))
        ],
      ),
    );
  }

  getData(text){
    return Container(
      margin: const EdgeInsets.only(bottom: 10.0, left: 10.0),
      child: Text(
        text,
        style: const TextStyle(
            fontSize: 15.0
        ),
      ),
    );
  }

  Widget getMapWidget(int treecount){

    List<int> map = [
      1,1,1,1,1,
      1,1,1,1,1,
      1,1,1,1,1,
      1,1,1,1,1,
      1,1,1,1,1,
    ];

    treecount = treecount>map.length?map.length:treecount;

    for(int i = 0;i<treecount;i++){
      map[i] += 10;
    }

    Size floorSize = const Size(47,36);
    Size treeSize = const Size(87,87);

    int rowCount = 5;

    List<Positioned> all = [];

    for(int i = 0;i<map.length;i++){
      int xIndex = (i/rowCount).floor();
      int yIndex = (i%rowCount).floor();

      int floorType = map[i]%10;

      all.add(Positioned(
          left: xIndex*floorSize.width/2+yIndex*floorSize.width/2+80,
          top:yIndex*floorSize.height/2-xIndex*floorSize.height/2+100+40,
          child: Image.asset("assets/floor$floorType.png",width: floorSize.width,height: floorSize.height)
      ));
    }
    for(int i = 0;i<map.length;i++){
      int xIndex = (i/rowCount).floor();
      int yIndex = (i%rowCount).floor();

      int treeType = ((map[i]%100)/10).floor();

      if(treeType>0){
        all.add(Positioned(
            left: xIndex*floorSize.width/2+yIndex*floorSize.width/2-treeSize.width*.3+80,
            top:yIndex*floorSize.height/2-xIndex*floorSize.height/2+135-treeSize.height+40,
            child: Image.asset("assets/youzi$treeType.png",width: treeSize.width,height: treeSize.height)
        ));
      }
      if(treeType>0){
        all.add(Positioned(
            left: xIndex*floorSize.width/2+yIndex*floorSize.width/2-treeSize.width*.3+40+80,
            top:yIndex*floorSize.height/2-xIndex*floorSize.height/2+135-treeSize.height+40+40,
            child: Image.asset("assets/o2.png",width: treeSize.width/4,height: treeSize.height/4)
        ));
      }
    }

    all.add(Positioned(
        left: floorSize.width/2+floorSize.width/2-treeSize.width*.3+180+80,
        top:floorSize.height/2-floorSize.height/2+135-treeSize.height,
        child: const Icon(
          Icons.wb_sunny,
          color: Colors.orange,
        )
    )
    );

    return Stack(
      children: all,
    );
  }

}
