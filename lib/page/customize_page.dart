import 'package:carbonrecord/bean/customize_activity.dart';
import 'package:carbonrecord/page/customize_selection_page.dart';
import 'package:carbonrecord/sql/sqlite_helper.dart';
import 'package:flutter/material.dart';

import '../bean/customize_activity_vehicle.dart';
import '../config/vehicle_icons.dart';

class CustomizePage extends StatefulWidget {
  const CustomizePage({Key? key}) : super(key: key);

  @override
  State<CustomizePage> createState() => _CustomizePageState();
}

class _CustomizePageState extends State<CustomizePage> {
  List<CustomizeActivity> customizeActivityList = [];
  Map<int, List<CustomizeActivityVehicle>> customizeActivityVehicleMap = {};

  // 监听新建活动
  var controller = TextEditingController();


  @override
  void initState() {
    _queryData();
  }

  Future _queryData() async {
    customizeActivityList = await SqliteHelper.instance.getCustomizeActivityList();
    customizeActivityVehicleMap = await SqliteHelper.instance.getCustomizeActivityVehicleMap(customizeActivityList);
    setState(() {});
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            title: const Text('自定义活动'),
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.of(context).pop(),
            ),
            centerTitle: true,
            // 添加自定义活动
            actions: [
              IconButton(
                  onPressed: () {
                    _createNewActivity();
                  },
                  icon: const Icon(Icons.add)
              )
            ],
          ),
          body: ListView.builder(
              itemCount: customizeActivityList.length,
              itemBuilder: (context1, index1){
                return Card(
                  color: Colors.white,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  clipBehavior: Clip.antiAlias,
                  elevation: 5,
                  margin: const EdgeInsets.all(5),
                  child: Container(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        getCardHeader(context1, index1),
                        getCardContent(context1, index1),
                      ],
                  ),
                  ),
                );
              }
          )
        );
  }

  Widget getCardHeader(BuildContext context, int index1){
    return ListTile(
      title: Container(
        alignment: Alignment.centerLeft,
        height: 50,
        child: Text(
          customizeActivityList[index1].activityName,
          style: const TextStyle(
              fontSize: 20.0,
              color: Colors.blue
          ),
        ),
      ),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          IconButton(
            onPressed: (){
              _showDeleteCustomizeDialog(0, customizeActivityList[index1].id, 1);
            },
            icon: const Icon(Icons.delete),
          ),
          IconButton(
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CustomizeSelectionPage(customizeWayId: customizeActivityList[index1].id)),
                ).then((value){
                  if(value){
                    _queryData();
                  }
                });
              },
              icon: const Icon(Icons.add)
          ),
        ],
      ),
    );
  }

  Widget getCardContent(BuildContext context, int index1){
    return ListView.builder(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: customizeActivityVehicleMap[customizeActivityList[index1].id]?.length,
        itemBuilder: (context2, index2){
          return ListTile(
            title: Text(customizeActivityVehicleMap[customizeActivityList[index1].id]![index2].vehicleName.toString()),
            leading: getLeadingIcon(customizeActivityVehicleMap[customizeActivityList[index1].id]![index2].vehicleName.toString()),
            trailing: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('  ${customizeActivityVehicleMap[customizeActivityList[index1].id]![index2].vehicleName.toString()}'),
                Text('  ${customizeActivityVehicleMap[customizeActivityList[index1].id]![index2].unitName.toString()}'),
                IconButton(
                    onPressed: (){
                      _showDeleteCustomizeDialog(customizeActivityVehicleMap[customizeActivityList[index1].id]![index2].id, customizeActivityList[index1].id, 0);
                    },
                    icon: const Icon(Icons.delete))
              ],
            ),
          );
        }
    );
  }

  _showDeleteCustomizeDialog(vehicleId, customizeWayId, isActivity){
    showDialog(
        context: context,
        builder: (context){
          return AlertDialog(
          title: const Text('是否删除该项？'),
          actions: [
            TextButton(
              onPressed: (){
                Navigator.of(context).pop();
                setState(() {});
              },
              child: const Text('取消'),
            ),
            TextButton(
              onPressed: () {
                if(isActivity == 1){
                  _deleteCustomizeWays(context, customizeWayId);
                }
                else{
                  _deleteCustomizeVehicles(context, vehicleId);
                }
                Navigator.of(context).pop();
                setState(() {});
              },
              child: const Text('确定'),
            )
          ],
        );
    });
  }

  _deleteCustomizeWays(BuildContext context, int customizeActivityId) async {
    SqliteHelper.instance.deleteCustomizeActivity(customizeActivityId);
    _queryData();
  }

  _deleteCustomizeVehicles(BuildContext context, int customizeVehicleId) async {
    SqliteHelper.instance.deleteCustomizeActivityVehicle(customizeVehicleId);
    _queryData();
  }

  Future<void> insertCustomizeWays(TextEditingController controller) async {
    SqliteHelper.instance.insertCustomizeActivityVehicle(controller.text);
    _queryData();
  }

  _createNewActivity() {
    showDialog(
        context: context,
        builder: (context){
          return AlertDialog(
            title: const Text('新建活动'),
            content: TextField(
              controller: controller,
              textInputAction: TextInputAction.done,
              textAlign: TextAlign.left,
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  icon: const Icon(Icons.close),
                  onPressed: () {
                    controller.clear();
                  },
                ),
              ),
            ),
            actions: [
              TextButton(
                onPressed: (){
                  Navigator.of(context).pop();
                  setState(() {});
                },
                child: const Text('取消'),
              ),
              TextButton(
                onPressed: () {
                  print(controller.text);
                  insertCustomizeWays(controller);
                  Navigator.of(context).pop();
                  setState(() {});
                },
                child: const Text('确定'),
              )
            ],
          );
        }
    );
  }
}
