import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';
import '../config/vehicle_icons.dart';
import '../utils/sqlite_handler.dart';


class CustomizeAddObjectPage extends StatefulWidget{
  int objectId;
  int customizeWayId;

  CustomizeAddObjectPage({Key? key, required this.customizeWayId, required this.objectId}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CustomizeAddObjectPageState();
  }
}

class _CustomizeAddObjectPageState extends State<CustomizeAddObjectPage>{
  List objectList = [];
  String inputValue = '0';
  double resultValue = 0;

  DateFormat df = DateFormat('yyyy-MM-dd HH:mm:ss');
  late DateTime _dateTime;

  var controller = TextEditingController();

  @override
  void initState() {
    _queryObjectData(widget.objectId);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: const Text('自定义活动项目'),
              centerTitle: true,
            ),
            body:objectList.isEmpty
                ? const Text('当前没有数据')
                :Column(
              // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ListTile(
                  leading: getLeadingIcon(objectList[0]['vehicle_name'].toString()),
                  title: Text('${objectList[0]['vehicle_name']}'),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text('碳排因子:'),
                      Text(' ${objectList[0]['factor']} '),
                      Text('kgCO2/${objectList[0]['unit_name']}'),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                SizedBox(
                  width: 330,
                  child: TextField(
                    controller: controller,
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.done,
                    textAlign: TextAlign.left,
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          controller.clear();
                        },
                      ),
                      border: const OutlineInputBorder(
                      ),
                      labelText: "输入量  单位：${objectList[0]['unit_name']}",
                    ),

                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[0-9.]'))
                    ],
                    onChanged: (text){
                      inputValue = text;
                      resultValue = double.parse(inputValue==''?'0':inputValue) * (objectList[0]['factor']);
                      setState(() {});
                    },
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  width: 330,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    border: Border.all(color: Colors.grey, width: 1),
                  ),
                  child: ListTile(
                    title: Container(
                      child: Text('总计   '+(resultValue.toStringAsFixed(5)+'   kgCO2')),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  width: 330,
                  child:  OutlinedButton.icon(
                    icon: Icon(Icons.add_box),
                    onPressed: () {
                      if (inputValue == '0'||inputValue == ''){
                        showDialogFunction(context);
                      }else{
                        _dateTime = DateTime.now();
                        _insertCustomizeVehicles(widget.objectId, double.parse(inputValue), widget.customizeWayId);
                        Navigator.of(context).pop(true);
                      }
                    },
                    label: const Text('添加'),
                  ),
                ),
                const SizedBox(height: 20),
              ],
            )
        )
    );
  }

  _queryObjectData(int objectId) async {
    var db = await SqliteHandler.getDB('carbon_transport.db');
    objectList = await db.rawQuery('SELECT vehicles.id,vehicles.vehicle_name,vehicles.factor, units.unit_name FROM vehicles LEFT JOIN units WHERE vehicles.id = ? and vehicles.unit_id = units.id;',[objectId]);
    setState(() {});
  }

  _insertCustomizeVehicles(int objectId,double distance,int customizeWayId) async {
    var db = await SqliteHandler.getDB('carbon_transport.db');
    await db.rawInsert('insert into customize_vehicles(vehicle_id, distance, customize_way_id) VALUES (?, ?, ?);',[objectId, distance,customizeWayId]);
    setState(() {});
    db = null;
  }

  void showDialogFunction(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("error"),
            content: const Text("请输入数量"),
            actions: [
              TextButton(
                child: const Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        }
    );
  }

}
