import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:flutter/services.dart';
import 'package:sqflite/sqflite.dart';
import '../config/vehicle_icons.dart';
import '../main.dart';
import '../utils/sqlite_handler.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'customize_record_selection_page.dart';

class AddcustomizePage extends StatefulWidget{
  int customize_id;
  String customize_name;

  AddcustomizePage({Key? key, required this.customize_id,required this.customize_name}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _AddcustomizePageState();
  }
}

class _AddcustomizePageState extends State<AddcustomizePage>{
  List customize_list = [];
  List now_customize_record_list = [];
  double customize_record_distance = 0;
  double customize_record_result = 0;
  double record_distance_result = 0;
  int flag = 0;

  DateFormat df = DateFormat('yyyy-MM-dd HH:mm:ss');
  late DateTime _dateTime;

  var controller = TextEditingController();



  @override
  void initState() {
    _query_customize_Data(widget.customize_id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text('碳添加'),
            centerTitle: true,
          ),
          body:customize_list.isEmpty
            ? const Text('当前没有数据')
            :Column(
              children: <Widget>[
                ListTile(
                  title: Text("${widget.customize_name}",
                    style: const TextStyle(
                      fontSize: 30.0,
                    ),
                  ),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text('碳排总计：'),
                      Text('    ${_get_total_result(customize_list).toStringAsFixed(3)}    '),
                      const Text('kgCO2'),
                    ],
                  ),
                ),
                SizedBox(height: 15,),
                Divider(
                  height: 1,
                  color: Colors.blue,
                ),
                IconButton(
                    onPressed: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => CustomizeRecordSelectionPage()),
                      ).then((value){
                        if(value.length!=0){
                          _add_customize_list(value);
                          setState(() {});
                        }
                      });
                    },
                    icon: Icon(Icons.add)
                ),
                SizedBox(height: 15,),
                Divider(
                  height: 1,
                  color: Colors.blue,
                ),
                Expanded(
                  child: (
                    ListView.separated(
                      shrinkWrap: true,
                      itemCount: customize_list.length,
                      itemBuilder: (context,index){
                        return Slidable(
                          endActionPane: ActionPane(
                            motion: const ScrollMotion(),
                            children: [
                              SlidableAction(
                                onPressed:(context)=>_del_list_index(index),
                                backgroundColor: const Color(0xFFFE4A49),
                                foregroundColor: Colors.white,
                                icon: Icons.delete,
                                label: '删除',
                              ),
                            ],
                          ),
                          child: ListTile(
                            leading: getLeadingIcon(customize_list[index]['vehicle_name'].toString()),
                            title: Text('${customize_list[index]['vehicle_name']}'),
                            subtitle: Row(
                              children: [
                                SizedBox(
                                  width: 40,
                                  child: Text('${customize_list[index]['distance']}'),
                                ),
                                Text('${customize_list[index]['unit_name']}'),
                              ],
                            ),
                            trailing: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text('${(customize_list[index]['distance']*customize_list[index]['factor']).toStringAsFixed(3)}    '),
                                const Text('kgCO2'),
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return const Divider(
                          color: Colors.blue,
                        );
                      },
                    )
                  ),
                ),
                Divider(
                  height: 1,
                  color: Colors.blue,
                ),
                SizedBox(height: 20,),
                Container(
                width: 330,
                child:OutlinedButton.icon(
                  icon: Icon(Icons.add_box),
                  onPressed: () {
                    _dateTime = DateTime.now();
                    _submit_data(customize_list,customize_record_result,record_distance_result,df.format(_dateTime),widget.customize_name);
                    Navigator.of(context).pop(true);
                  },
                  label: const Text('提交至记录'),
                  ),
                ),
                SizedBox(height: 30,),

              ],
            )

    );
  }

  _query_customize_Data(int customize_id) async {
    var db = await SqliteHandler.getDB('carbon_transport.db');
    customize_list = await db.rawQuery('SELECT c.distance,v2.vehicle_name,v2.factor,v2.unit_name,v2.id as vehicle_id  FROM customize_vehicles as c left JOIN (SELECT v.id,v.vehicle_name,v.factor,u.unit_name FROM vehicles as v LEFT JOIN units as u WHERE v.unit_id = u.id) as v2 where c.vehicle_id = v2.id and c.customize_way_id = ?;',[customize_id]);
    setState(() {});
  }

  _get_total_result(List customize_list) {
    setState(() {
      customize_record_result = 0;
      record_distance_result = 0;
      for (var temp in customize_list){
        customize_record_result += temp['distance']*temp['factor'];
        record_distance_result += temp['distance'];
      }
    });
    return customize_record_result;
  }

  _del_list_index(int index){
    setState(() {
      customize_list = List.from(customize_list)..removeAt(index);
    });
  }

  _submit_data(List customize_list,double customize_record_result,double record_distance_result,String datetime,String customize_name) async {
    var db = await SqliteHandler.getDB('carbon_transport.db');
    await db.rawInsert("insert into customize_records(emission_result,distance,create_time,update_time,customize_name) values(?,?,?,?,?)",[customize_record_result,record_distance_result,datetime,datetime,customize_name]);
    List temp2 = await db.rawQuery("SELECT id FROM customize_records WHERE update_time = ?",[datetime]);
    int customize_id = temp2[0]['id'];
    for (var temp in customize_list){
      double emission_result = temp['distance']*temp['factor'];
      await db.rawInsert('insert into records(vehicle_id,emission_result,distance,customize_id,create_time,update_time) values(?,?,?,?,?,?)',[temp['vehicle_id'],emission_result,temp['distance'],customize_id,datetime,datetime]);
    }
    setState(() {});
    // await db.close();
  }

  void showDialogFunction(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("error"),
            content: const Text("请输入数量"),
            actions: [
              TextButton(
                child: const Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        }
      );
  }

  _add_customize_list(Map<String,Object?> map){
    customize_list = List.from(customize_list);
    customize_list.add(map);
    setState(() {});
  }

}
