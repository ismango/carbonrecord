import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter/services.dart';
import '../utils/sqlite_handler.dart';


class RecordUpdatePage extends StatefulWidget{
  int recordId;

  RecordUpdatePage({Key? key, required this.recordId}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _RecordUpdatePageState();
  }
}

class _RecordUpdatePageState extends State<RecordUpdatePage>{
  List objectList = [];
  String inputValue = '0';
  double resultValue = 0;

  DateFormat df = DateFormat('yyyy-MM-dd HH:mm:ss');

  var controller = TextEditingController();

  Widget getLeadingIcon(String vehicle){
    switch(vehicle){
      case '公交车':
        return const Icon(Icons.directions_bus);
      case '小汽车':
        return const Icon(Icons.car_rental);
      case '摩托车':
        return const Icon(Icons.motorcycle);
      case '飞机':
        return const Icon(Icons.airplanemode_active);
      case '火车':
        return const Icon(Icons.train);
      case '高铁':
        return const Icon(Icons.directions_railway);
      case '地铁':
        return const Icon(Icons.subway);
      case '轻轨':
        return const Icon(Icons.directions_subway);
      case '轮船':
        return const Icon(Icons.directions_boat);
      default:
        return const Icon(Icons.dashboard_customize);
    }
  }

  @override
  void initState() {
    _queryObjectData(widget.recordId);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
              leading: IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: const Text('碳记录更新'),
              centerTitle: true,
            ),
            body:objectList.isEmpty
                ? const Text('当前没有数据')
                :Column(
              children: <Widget>[
                _showRecordTitle(),
                const SizedBox(height: 20),
                SizedBox(
                  width: 330,
                  child: TextField(
                    controller: controller,
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.done,
                    textAlign: TextAlign.left,
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          controller.clear();
                        },
                      ),
                      border: const OutlineInputBorder(
                      ),
                      labelText: "输入量  单位：${objectList[0]['unit_name']}",
                    ),

                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[0-9.]'))
                    ],
                    onChanged: (text){
                      inputValue = text;
                      resultValue = double.parse(inputValue==''?'0':inputValue) * (objectList[0]['factor']);
                      setState(() {});
                    },
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  width: 330,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    border: Border.all(color: Colors.grey, width: 1),
                  ),
                  child: ListTile(
                    title: Container(
                      child: Text('总计   '+(resultValue.toStringAsFixed(5)+'   kgCO2')),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  width: 330,
                  child:  OutlinedButton.icon(
                    icon: Icon(Icons.add_box),
                    onPressed: () {
                      if (inputValue == '0'||inputValue == ''){
                        showDialogFunction(context);
                      }else{
                        _updateRecord(double.parse(inputValue), resultValue, widget.recordId);
                        Navigator.of(context).pop(true);
                      }
                    },
                    label: const Text('修改记录'),
                  ),
                ),
                const SizedBox(height: 20),
              ],
            )
        )
    );
  }

  _queryObjectData(int recordId) async {
    var db = await SqliteHandler.getDB('carbon_transport.db');
    objectList = await db.rawQuery('select r.id, v.vehicle_name, v.factor, r.distance, r.emission_result, u.unit_name from records r, vehicles v, units u where r.vehicle_id = v.id and v.unit_id = u.id and r.customize_id = 0 and r.id = ?;',[recordId]);
    setState(() {});
  }

  _updateRecord(double distance, double resultValue, int recordId) async {
    var db = await SqliteHandler.getDB('carbon_transport.db');
    await db.rawUpdate('update records set distance = ?, emission_result = ?, update_time=? where id = ?;',[distance, resultValue, df.format(DateTime.now()), recordId]);
    setState(() {});
    db = null;
  }

  void showDialogFunction(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("error"),
            content: const Text("请输入数量"),
            actions: [
              TextButton(
                child: const Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        }
    );
  }

  Widget _showRecordTitle(){
    return ListTile(
      leading: getLeadingIcon(objectList[0]['vehicle_name'].toString()),
      title: Text('${objectList[0]['vehicle_name']}'),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text('碳排因子:'),
          Text(' ${objectList[0]['factor']} '),
          Text('kgCO2/${objectList[0]['unit_name']}'),
        ],
      ),
    );
  }
}
