// import 'package:carbonrecord/page/activity_page.dart';
// import 'package:carbonrecord/page/update_page.dart';
import 'package:carbonrecord/bean/record.dart';
import 'package:carbonrecord/bean/total_average.dart';
import 'package:carbonrecord/page/customize_page.dart';
import 'package:carbonrecord/page/record_update_page.dart';
import 'package:carbonrecord/page/selection_page.dart';
import 'package:carbonrecord/sql/sqlite_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';

import '../config/vehicle_icons.dart';
import 'morecustomize_page.dart';

class RecordPage extends StatefulWidget {
  const RecordPage({Key? key}) : super(key: key);

  @override
  State<RecordPage> createState() => _RecordPageState();
}

class _RecordPageState extends State<RecordPage> {
  List<Record> records = [];
  TotalAverage totalAverage =
      TotalAverage(totalEmissionResult: 0, averageEmissionResult: 0, treeCount: 0);

  DateFormat dateFormat = DateFormat('yyyy-MM-dd');

  DateTime endTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day)
          .add(const Duration(hours: 23, minutes: 59, seconds: 59));
  DateTime startTime =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day)
          .add(const Duration(hours: 23, minutes: 59, seconds: 59))
          .add(const Duration(days: -30, seconds: 1));

  @override
  void initState() {
    _queryData();
  }

  _queryData() async {
    records.clear();
    records = await SqliteHelper.instance.getRecordList(startTime, endTime);

    totalAverage =
        await SqliteHelper.instance.getTotalAverage(startTime, endTime);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _showTimePicker(),
        centerTitle: true,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const CustomizePage()),
                );
              },
              icon: const Icon(Icons.dashboard_customize))
        ],
      ),
      body: Column(
        children: [
          records.isEmpty
              ? const Center(
                  child: Text('当前没有数据'),
                )
              : Expanded(
                  child: ListView.builder(
                  shrinkWrap: true,
                  // scrollDirection: Axis.vertical,
                  itemCount: records.length + 1,
                  itemBuilder: (context, index) {
                    if (index == 0) {
                      return _getHeadImage();
                    } else {
                      index -= 1;
                      return _getRecord(context, index);
                    }
                  },
                )),
        ],
      ),
      floatingActionButton: _getFloatingActionButton(),
    );
  }

  Widget _getFloatingActionButton() {
    return FloatingActionButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => const SelectionPage()),
        ).then((value) {
          Future.delayed(const Duration(milliseconds: 300), () {
            _queryData();
          });
        });
      },
      tooltip: 'record',
      child: const Icon(Icons.add),
    );
  }

  _deleteRecordOrActivity(BuildContext context, int index) async {
    SqliteHelper.instance.deleteRecordOrActivity(records[index]);
    records = await SqliteHelper.instance.getRecordList(startTime, endTime);
    setState(() {});
  }

  Future showTimeConfirmDialog() {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('提示'),
            content: const Text('开始时间不能晚于结束日期'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(() {});
                },
                child: const Text('确定'),
              )
            ],
          );
        });
  }

  _showTimePicker() {
    return Container(
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          TextButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    showTitleActions: true,
                    maxTime: DateTime.now(), onConfirm: (date) {
                  if (date.isAfter(endTime)) {
                    showTimeConfirmDialog();
                  } else {
                    startTime = DateTime(date.year, date.month, date.day);
                    _queryData();
                    setState(() {});
                  }
                }, currentTime: startTime, locale: LocaleType.zh);
              },
              child: Text(
                dateFormat.format(startTime),
                style: const TextStyle(color: Colors.white, fontSize: 18.0),
              )),
          // const Text(':'),
          // 结束时间
          TextButton(
              onPressed: () {
                DatePicker.showDatePicker(context,
                    showTitleActions: true,
                    maxTime: DateTime.now(), onConfirm: (date) {
                  if (date.isBefore(startTime)) {
                    showTimeConfirmDialog();
                  } else {
                    endTime = DateTime(date.year, date.month, date.day).add(
                        const Duration(hours: 23, minutes: 59, seconds: 59));
                    _queryData();
                    setState(() {});
                  }
                }, currentTime: endTime, locale: LocaleType.zh);
              },
              child: Text(
                dateFormat.format(endTime),
                style: const TextStyle(color: Colors.white, fontSize: 18.0),
              ))
        ],
      ),
    );
  }

  _showDeleteDialog(BuildContext context, int index) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text('是否删除该项？'),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  setState(() {});
                },
                child: const Text('取消'),
              ),
              TextButton(
                onPressed: () {
                  _deleteRecordOrActivity(context, index);
                  Navigator.of(context).pop();
                  setState(() {});
                },
                child: const Text('确定'),
              )
            ],
          );
        });
  }

  Widget _getData(text) {
    return Container(
      margin: const EdgeInsets.only(left: 10.0, bottom: 10.0),
      child: Text(
        text,
        style: const TextStyle(color: Colors.white, fontSize: 18.0),
      ),
    );
  }

  Widget _getHeadImage() {
    return Container(
      height: 200,
      width: double.infinity,
      margin: const EdgeInsets.only(bottom: 6.0),
      // alignment: Alignment.bottomLeft,
      decoration: const BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/record.jfif"), fit: BoxFit.fill)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            children: [
              _getData("总碳排量"),
              _getData(
                  "${totalAverage.totalEmissionResult.toStringAsFixed(2)}  kg"),
            ],
          ),
          Row(
            children: [
              _getData("日均碳排量"),
              _getData(
                  "${totalAverage.averageEmissionResult.toStringAsFixed(2)}  kg"),
            ],
          )
        ],
      ),
    );
  }

  Widget _getRecord(BuildContext context, int index) {
    return Card(
        color: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
        ),
        clipBehavior: Clip.antiAlias,
        elevation: 2,
        margin: const EdgeInsets.all(6.0),
        child: Slidable(
            startActionPane: records[index].customizeId == -1
                ? ActionPane(motion: const ScrollMotion(), children: [
                    SlidableAction(
                      onPressed: (BuildContext context) {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => RecordUpdatePage(
                                    recordId: records[index].id,
                                  )),
                        ).then((value) {
                          _queryData();
                        });
                      },
                      backgroundColor: const Color(0xFF21B7CA),
                      foregroundColor: Colors.white,
                      icon: Icons.update,
                      label: '更新',
                      // padding: const EdgeInsets.only(top: 0,bottom: 0),
                    )
                  ])
                : null,
            endActionPane: ActionPane(motion: const ScrollMotion(), children: [
              SlidableAction(
                onPressed: (context) => _showDeleteDialog(context, index),
                // deleteRecordOrActivity(context, index),
                backgroundColor: const Color(0xFFFE4A49),
                foregroundColor: Colors.white,
                icon: Icons.delete,
                label: '删除',
              ),
            ]),
            child: Container(
              alignment: Alignment.center,
              height: 80,
              child: ListTile(
                leading: getLeadingIcon(records[index].vehicleName.toString()),
                title: Text(dateFormat.format(dateFormat.parse(records[index].updateTime))),
                subtitle: Row(
                  children: [
                    SizedBox(
                      width: 60,
                      child: Text(records[index].vehicleName),
                    ),
                    Text('  ${records[index].distance}'),
                    Text('  ${records[index].unitName}'),
                  ],
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(records[index].emissionResult.toStringAsFixed(2)),
                    const Text(' kgCO2'),
                  ],
                ),
                onTap: () {
                  if (records[index].customizeId != -1) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MorecustomizePage(
                                customize_record_id: records[index].customizeId,
                              )),
                    ).then((value) {
                      _queryData();
                    });
                  }
                },
              ),
            )));
  }
}
