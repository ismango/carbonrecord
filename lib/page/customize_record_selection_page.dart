import 'dart:io';

import 'package:carbonrecord/page/addcustomize_page.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import '../utils//sqlite_handler.dart';
import 'addobject_page.dart';
import 'customize_record_add_object_page.dart';

class CustomizeRecordSelectionPage extends StatefulWidget {
  const CustomizeRecordSelectionPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CustomizeRecordSelectionPageState();
}

class _CustomizeRecordSelectionPageState extends State<CustomizeRecordSelectionPage> {

  List categories_list = [];
  Map<int,List> object_map = {};

  @override
  void initState() {
    _query_object();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).pop(true),
          ),
          title: const Text('分类'),
          centerTitle: true,
        ),
        body: Column(
          children: <Widget>[
            categories_list.isEmpty
            ?Text('当前没有数据！',textAlign: TextAlign.center)
            :Container(
              child: (
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: categories_list.length,
                  itemBuilder: (context,index){
                    return ExpansionTile(
                      title: Text('${categories_list[index]['category_name']}'),
                      children: [
                        ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: object_map[categories_list[index]['id']]?.length,
                          itemBuilder: (context,index2){
                            return ListTile(
                              title: Text('${object_map[categories_list[index]['id']]![index2]['vehicle_name']}',textAlign: TextAlign.center),
                              onTap: ()async {
                                final Map<String,Object?> value = await Navigator.of(context).push(
                                  MaterialPageRoute(builder: (context) => CustomizeRecordAddobjectPage(objectid: object_map[categories_list[index]['id']]![index2]['id'],)),);
                                  if (value.isNotEmpty){
                                    Navigator.of(context).pop(value);
                                  }
                              },
                            );
                          },
                        )
                      ],
                    );
                  },
                )
              ),
            ),

          ],
        )

    );
  }


  //从缓存目录读取db文件里面的数据
  _query_object() async {
    var db = await SqliteHandler.getDB('carbon_transport.db');
    categories_list = await db.rawQuery('SELECT * FROM categories;');
    for (var category_map_temp in categories_list){
      int category_id_temp = category_map_temp['id'];
      object_map[category_id_temp] = await db.rawQuery('SELECT * FROM vehicles WHERE category_id = ?;', [category_id_temp]);
    }
    setState(() {});
    // await db.close();
  }
}