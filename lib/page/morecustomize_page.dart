import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:flutter/services.dart';
import 'package:sqflite/sqflite.dart';
import '../config/vehicle_icons.dart';
import '../main.dart';
import '../utils/sqlite_handler.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MorecustomizePage extends StatefulWidget{
  int customize_record_id;

  MorecustomizePage({Key? key, required this.customize_record_id}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MorecustomizePageState();
  }
}

class _MorecustomizePageState extends State<MorecustomizePage>{
  List customize_list = [];
  List record_list = [];


  @override
  void initState() {
    _query_customize_Data(widget.customize_record_id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text('碳计算'),
            centerTitle: true,
          ),
          body:customize_list.isEmpty
            ? const Text('当前没有数据')
            :Column(
              children: <Widget>[
                _showCustomize(),
                const SizedBox(height: 15,),
                const Divider(height: 1, color: Colors.red,),
                _showRecordlist()
              ],
            )
        )
    );
  }

  _query_customize_Data(int customize_id) async {
    var db = await SqliteHandler.getDB('carbon_transport.db');
    customize_list = await db.rawQuery('SELECT * FROM customize_records WHERE id = ?;',[customize_id]);
    record_list = await db.rawQuery('SELECT r.vehicle_id,r.emission_result,r.distance,r.update_time,v2.vehicle_name,v2.unit_name FROM records as r left JOIN (SELECT v.id,v.vehicle_name,v.factor,u.unit_name FROM vehicles as v LEFT JOIN units as u WHERE v.unit_id = u.id) as v2 where r.customize_id = ? and r.vehicle_id = v2.id;;',[customize_id]);
    setState(() {});
  }

  Widget _showCustomize(){
    return ListTile(
      title: Text("   ${customize_list[0]['customize_name']}",
        style: const TextStyle(
          fontSize: 30.0,
        ),
      ),
      subtitle: Row(
        children: [
          Text('  ${customize_list[0]['update_time']}'),
        ],
      ),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text('碳排总计：'),
          Text(' ${customize_list[0]['emission_result'].toStringAsFixed(3)} '),
          const Text('kgCO2'),
        ],
      ),
    );
  }

  Widget _showRecordlist(){
    return Expanded(
      child: (
          ListView.separated(
            shrinkWrap: true,
            itemCount: record_list.length,
            itemBuilder: (context,index){
              return ListTile(
                leading: getLeadingIcon(record_list[index]['vehicle_name'].toString()),
                title: Text('${record_list[index]['vehicle_name']}'),
                subtitle: Row(
                  children: [
                    SizedBox(
                      width: 40,
                      child: Text('${record_list[index]['distance']}'),
                    ),
                    Text('${record_list[index]['unit_name']}'),
                  ],
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text('${(record_list[index]['emission_result']).toStringAsFixed(3)}    '),
                    const Text('kgCO2'),
                  ],
                ),
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return const Divider(
                color: Colors.blue,
              );
            },
          )
      ),
    );
  }

}
