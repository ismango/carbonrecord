import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:flutter/services.dart';
import 'package:sqflite/sqflite.dart';
import '../config/vehicle_icons.dart';
import '../main.dart';
import '../utils/sqlite_handler.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AddobjectPage extends StatefulWidget{
  int objectid;

  AddobjectPage({Key? key, required this.objectid}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _AddobjectPageState();
  }
}

class _AddobjectPageState extends State<AddobjectPage>{
  List object_list = [];
  String input_value = '0';
  double result_value = 0;

  DateFormat df = DateFormat('yyyy-MM-dd HH:mm:ss');
  late DateTime _dateTime;

  var controller = TextEditingController();


  @override
  void initState() {
    _query_object_Data(widget.objectid);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              icon: const Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: const Text('碳添加'),
            centerTitle: true,
          ),
          body:object_list.isEmpty
            ? const Text('当前没有数据')
            :Column(
              // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ListTile(
                  leading: getLeadingIcon(object_list[0]['vehicle_name'].toString()),
                  title: Text('${object_list[0]['vehicle_name']}'),
                  trailing: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text('碳排因子:'),
                      Text(' ${object_list[0]['factor']} '),
                      Text('kgCO2/${object_list[0]['unit_name']}'),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                SizedBox(
                  width: 330,
                  child: TextField(
                    controller: controller,
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.done,
                    textAlign: TextAlign.left,
                    decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: const Icon(Icons.close),
                        onPressed: () {
                          controller.clear();
                        },
                      ),
                      border: const OutlineInputBorder(
                      ),
                      labelText: "输入量  单位：${object_list[0]['unit_name']}",
                    ),

                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp('[0-9.]'))
                    ],
                    onChanged: (text){
                      input_value = text;
                      result_value = double.parse(input_value==''?'0':input_value) * (object_list[0]['factor']);
                      setState(() {});
                    },
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  width: 330,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(4.0)),
                    border: Border.all(color: Colors.grey, width: 1),
                  ),
                  child: ListTile(
                    title: Container(
                      child: Text('总计   '+(result_value.toStringAsFixed(5)+'   kgCO2')),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                Container(
                  width: 330,
                  child:  OutlinedButton.icon(
                    icon: Icon(Icons.add_box),
                    onPressed: () {
                      if (input_value == '0'||input_value == ''){
                        showDialogFunction(context);
                      }else{
                        _dateTime = DateTime.now();
                        _submit_object_record(object_list[0]['id'],result_value,input_value,df.format(_dateTime));
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => const MyApp()),
                        );
                      }
                    },
                    label: const Text('提交至记录'),
                  ),
                ),
                const SizedBox(height: 20),
              ],
            )
        )
    );
  }

  _query_object_Data(int ObjectId) async {
    var db = await SqliteHandler.getDB('carbon_transport.db');
    object_list = await db.rawQuery('SELECT vehicles.id,vehicles.vehicle_name,vehicles.factor, units.unit_name FROM vehicles LEFT JOIN units WHERE vehicles.id = ? and vehicles.unit_id = units.id;',[ObjectId]);
    setState(() {});
    // await db.close();
  }

  _submit_object_record(int ObjectId,double emission_re,String input_value,String datatime) async {
    var db = await SqliteHandler.getDB('carbon_transport.db');
    await db.rawInsert("insert into records(vehicle_id,emission_result,distance,create_time,update_time,customize_id) values(?,?,?,?,?,?)",[ObjectId,emission_re,double.parse(input_value),datatime,datatime,0]);
    setState(() {});
    // await db.close();
  }

  void showDialogFunction(BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: const Text("error"),
            content: const Text("请输入数量"),
            actions: [
              TextButton(
                child: const Text("OK"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        }
      );
  }

}
