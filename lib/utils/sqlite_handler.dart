import 'dart:io';
import 'package:flutter/services.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';


class SqliteHandler{
   static getDB(String database) async {
     var databasesPath = await getDatabasesPath();
     var exist = await File(databasesPath+'/'+database).exists();
     var path = join(databasesPath, database);

    // var exist = true;
     if (!exist) {
       await deleteDatabase(path);

       try {
         await Directory(dirname(path)).create(recursive: true);
       } catch (_) {}

       var data = await rootBundle.load(join('assets', database));

       List<int> bytes = data.buffer.asUint8List(
           data.offsetInBytes, data.lengthInBytes);
       await File(path).writeAsBytes(bytes, flush: true);
     }
     var db = await openDatabase(path);
     return db;
  }


}